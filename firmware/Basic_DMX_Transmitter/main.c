/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__PIC24E__)
	#include <p24Exxxx.h>
#elif defined(__PIC24F__)
	#include <p24Fxxxx.h>
#elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
#endif

_CONFIG1(JTAGEN_OFF & GCP_OFF & GWRP_ON & BKBUG_ON & ICS_PGx1
        & FWDTEN_OFF & WINDIS_ON & FWPSA_PR32 & WDTPS_PS32768);
_CONFIG2(IESO_ON & WUTSEL_LEG & SOSCSEL_SOSC & FNOSC_FRCPLL
        & FCKSM_CSDCMD & OSCIOFNC_ON & IOL1WAY_OFF & I2C1SEL_PRI & POSCMOD_NONE);


#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>       /* Includes true/false definition                  */

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp              */

/******************************************************************************/
/* Global Variable Declaration                                                */
/******************************************************************************/

// Define DMX values here
//uint8_t dmx_values[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
#define START_BYTE 0
#define TOP_R 0
#define TOP_G 255
#define TOP_B 0

#define BOT_R 0
#define BOT_G 255
#define BOT_B 255

#define R 0
#define G 1
#define B 2

#define NUM_STRIPS 3
#define NUM_COLORS 3

//uint8_t dmx_values[] = {0, 255, 255, TOP_R, TOP_G, TOP_B, BOT_R, BOT_G, BOT_B, 0, 0, 0};
uint8_t dmx_values[NUM_STRIPS*NUM_COLORS];

uint8_t dmx_rotation[][NUM_STRIPS][NUM_COLORS] = {
    {
        {0, 200, 0},
        {0, 200, 0},
        {0, 200, 0}
    },
    {
        {255, 0, 0},
        {255, 0, 0},
        {255, 0, 0}
    },
    {
        {255, 0, 0},
        {255, 0, 0},
        {255, 0, 0}
    },
    {
        {0, 0, 200},
        {0, 0, 200},
        {0, 0, 200}
    }
    /*
    {
        {125, 0, 80},
        {0, 0, 0},
        {125, 0, 80}
    },
    {
        {125, 100, 0},
        {125, 100, 0},
        {125, 100, 0}
    },
    {
        {125, 100, 0},
        {125, 100, 0},
        {125, 100, 0}
    },
    {
        {0, 0, 255},
        {0, 255, 0},
        {255, 0, 0}
    }*/
};

uint16_t num_dmx_rot = sizeof(dmx_rotation)/(9*sizeof(uint8_t));
#define NEXT(val) ((val == num_dmx_rot-1) ? 0 : val+1)
#define PREV(val) ((val == 0) ? num_dmx_rot-1 : val-1)

uint16_t rot_counter = 0;
uint8_t rot_update = 0;
uint8_t rot_period_c = 255;
uint16_t counter = 0;
uint16_t dmx_progress = 0;
uint16_t num_dmx_values = NUM_STRIPS*NUM_COLORS + 1;//sizeof(dmx_values)/sizeof(uint8_t);
uint8_t break_sent = 0; 

uint16_t voltage = 0;
uint8_t value = 0;

/****************************************************************************/

int16_t main(void)
{
    
    /*
     * Set clock speed to 32MHz => instruction clock to 16MHz
     */
    CLKDIVbits.RCDIV = 0b000;   //Prescaler 1: CLKDIV<10:8> Default: 001 = 4Mhz output | 000 = 8Mhz
    CLKDIVbits.DOZEN = 0b0; // Enable prescaler
    //CLKDIVbits.DOZE = 0b010;    //Prescaler 2: CLKDIR<14:12> Default: 011 = 1:8 | 000 = 1:1
    OSCCON = 0x11C0;	 //select INTERNAL RC, Post Scale PPL

    /* Initialize IO ports and peripherals */
    AD1PCFG = 0xFFFF; // Disable Analog inputs
    TRISB = 0X0000; // All pins output
    TRISA = 0X0000; // All pins output

    /* Configure UART */
    RPOR1bits.RP3R = 3;	 //UART1 transmit set to RB3
    U1BRG = 3; //set baud speed
    U1MODE = 0x8001; //turn on module
    //U1STA = 0x8400; //set interrupts => Set interrupt if buffer is empty
    U1STA = 0x0400; //set interrupts => Set interrupt if char is sent

     /* Configure timer 1 */
    PR1 = 0x056A;

    IPC0bits.T1IP = 5;	 //set interrupt priority
    IFS0bits.T1IF = 0;	 //reset interrupt flag
    IEC0bits.T1IE = 1;	 //turn on the timer1 interrupt
    T1CONbits.TON = 0;   //turn OFF the timer

    /* Configure timer 2 */
    //PR2 = 0x056A;
    //PR2 = value << 7;
    IPC1bits.T2IP = 2;	 //set interrupt priority
    IFS0bits.T2IF = 0;	 //reset interrupt flag
    IEC0bits.T2IE = 1;	 //turn on the timer1 interrupt
    //T2CONbits.TCKPS = 0b11; // Set prescaler to 1:256
    T2CONbits.TCKPS = 0b01; // Set prescaler to 1:2
    T2CONbits.TON = 1;   //turn ON the timer


    //reset RX interrupt flag
    INTCON2 = 0x0000;    // Setup INT0, INT1, INT2, interupt on falling edge
    IFS0bits.U1TXIF = 0; // Reset flag
    IPC3bits.U1TXIP = 1; // Set low priority

    IEC0bits.U1TXIE = 1;    /*Enable UART1TX Interrupt Service Routine */

    LATBbits.LATB0 = 1;
    LATBbits.LATB1 = 0;
    LATAbits.LATA1 = 1;

    // Start with a break, initialize all the packages
    dmx_progress = 0; // Reset the counter
    U1STAbits.UTXEN = 0; // Disable UART transmit pin
    LATBbits.LATB3 = 0; // Clear the transmit pin
    T1CONbits.TON = 1;   //turn on timer 1

    /* AD Conversion init*/
    //AD1CON1 = 0x80E4; //Turn on, auto sample start, auto-convert
    //AD1CON2 = 0x0000; //Vref+ (vdd), Vref- (vss), int every conversion, MUXA only
    //AD1CON3 = 0x1F05; //31 Tad auto-sample, Tad = 5*Tcy

    //AD1CHS = 0; // Connect AN0 to AD hardware
    //AD1PCFGbits.PCFG0 = 0;  //Disable digital input on AN0
    //AD1CSSL = 0;	 //No scanned inputs

    // Initialise values
    dmx_values[0] = dmx_rotation[rot_counter][0][R];
    dmx_values[1] = dmx_rotation[rot_counter][0][G];
    dmx_values[2] = dmx_rotation[rot_counter][0][B];

    dmx_values[3] = dmx_rotation[rot_counter][1][R];
    dmx_values[4] = dmx_rotation[rot_counter][1][G];
    dmx_values[5] = dmx_rotation[rot_counter][1][B];

    dmx_values[6] = dmx_rotation[rot_counter][2][R];
    dmx_values[7] = dmx_rotation[rot_counter][2][G];
    dmx_values[8] = dmx_rotation[rot_counter][2][B];

    while(1)
    {
        // Reading Analog values
        /*
        while (!AD1CON1bits.DONE);
        voltage = ADC1BUF0;
        value = (uint8_t)(voltage>>2);
         */

        // Counter acts as debugger
        if(counter == 10000)
        {
            LATBbits.LATB0 = ~LATBbits.LATB0;
            counter = 0;
        }

        if(rot_update == 1)
        {
            rot_update = 0;
            rot_period_c++;
            if(rot_period_c == 0)
                rot_counter = NEXT(rot_counter);
            uint16_t prev_rot_counter = PREV(rot_counter);

            // Fading routine
            int i, j, k = 0;
            for(i=0; i < NUM_STRIPS; i++)
            {
                for(j=0; j < NUM_COLORS; j++)
                {
                    if(dmx_rotation[rot_counter][i][j] > dmx_rotation[prev_rot_counter][i][j])
                    {
                        dmx_values[k] =
                                (dmx_values[k] == dmx_rotation[rot_counter][i][j])?
                                    dmx_values[k]:
                                    ++dmx_values[k];
                    }
                    else
                    {
                        dmx_values[k] =
                                (dmx_values[k] == dmx_rotation[rot_counter][i][j])?
                                    dmx_values[k]:
                                    --dmx_values[k];
                    }
                    k++;
                }
            }
        }
    }
}

//Interrupt routine
void __attribute__((__interrupt__, auto_psv)) _U1TXInterrupt(void)
{
    if(dmx_progress < num_dmx_values)
    {
        // Transmit a byte
        U1TXREG = dmx_values[dmx_progress++];
    }
    else
    {
        // Restart DMX procedure from the Break
        LATBbits.LATB3 = 0; // Clear the transmit pin
        U1STAbits.UTXEN = 0; // Disable UART transmit pin
        
        PR1 = 0x056A;       // Set 88�S time
        T1CONbits.TON = 1;   //turn on the timer
        LATBbits.LATB1 = 1;
        dmx_progress = 0; // Reset the counter
    }
    counter++; // Debug counter
    IFS0bits.U1TXIF = 0;    //Clear the U1TX interrupt flag
}

//_T1Interrupt() is the T1 interrupt service routine (ISR).
void __attribute__((__interrupt__, auto_psv)) _T1Interrupt(void)
{
    if(break_sent == 0)
    { // 88�S break has been sent
        LATBbits.LATB3 = 1;
        break_sent = 1;
        PR1 = 0x007E;   // Set 8�S MAB time
    }
    else
    { // 8�S MAB has been sent
        break_sent = 0;
        T1CONbits.TON = 0;   //turn off the timer

        U1STAbits.UTXEN = 1; // Enable UART transmit pin
        U1TXREG = START_BYTE; //dmx_values[0];
        dmx_progress = 0; // Fire start byte into UART

        LATBbits.LATB1 = 0;	//Toggle output to LED
    }
    IFS0bits.T1IF = 0; // Clear timer interrupt flag
}

//_T2Interrupt() is the T2 interrupt service routine (ISR).
void __attribute__((__interrupt__, auto_psv)) _T2Interrupt(void)
{
    LATAbits.LATA1 = ~LATAbits.LATA1;	//Toggle output to LED
    rot_update = 1;
    PR2 = 0xFFFF;
    IFS0bits.T2IF = 0; // Clear timer interrupt flag
}
