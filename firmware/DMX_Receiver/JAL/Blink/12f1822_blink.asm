#include <p12F1822.inc>

                                list p=12f1822, r=dec
                                errorlevel -306 ; no page boundary warnings
                                errorlevel -302 ; no bank 0 warnings
                                errorlevel -202 ; no 'argument out of range' warnings

                             __config 0x8007, 0x3fe4 ;0011 1111 1110 0100
                             ;Fail-safe-monitor: OFF
                             ;IESO: OFF
                             ;Clock out: disabled
                             ;BORen: Enabled
                             ;CPD: disabled

                             ;Code protect: Disabled
                             ;MCLR: pull up
                             ;Powerup timer: disabled
                             ;WDT: Disabled
                             ;OSC: INTOsc
                             __config 0x8008, 0x1eff ;111 101 111 11 11
                             ; LVP: enabled
                             ; debug: off
                             ; BORV: 1,9V
                             ; PLLEN: Disabled
                             ; WRT: protection off
v_true                         EQU 1
v_false                        EQU 0
v_on                           EQU 1
v_off                          EQU 0
v_output                       EQU 0
v__status                      EQU 0x0003  ; _status
v__z                           EQU 2
v_intcon                       EQU 0x000b  ; intcon
v_intcon_gie                   EQU 0x000b  ; intcon_gie-->intcon:7
v_pir1                         EQU 0x0011  ; pir1
v_pir1_txif                    EQU 0x0011  ; pir1_txif-->pir1:4
v_trisa                        EQU 0x008c  ; trisa
v_pin_a0_direction             EQU 0x008c  ; pin_a0_direction-->trisa:0
v_pie1                         EQU 0x0091  ; pie1
v_pie1_rcie                    EQU 0x0091  ; pie1_rcie-->pie1:5
v_pie1_txie                    EQU 0x0091  ; pie1_txie-->pie1:4
v_osccon                       EQU 0x0099  ; osccon
v_adcon0                       EQU 0x009d  ; adcon0
v_adcon1                       EQU 0x009e  ; adcon1
v_lata                         EQU 0x010c  ; lata
v_cm1con0                      EQU 0x0111  ; cm1con0
v_cm1con1                      EQU 0x0112  ; cm1con1
v_apfcon                       EQU 0x011d  ; apfcon
v_apfcon_rxdtsel               EQU 0x011d  ; apfcon_rxdtsel-->apfcon:7
v_apfcon_txcksel               EQU 0x011d  ; apfcon_txcksel-->apfcon:2
v_ansela                       EQU 0x018c  ; ansela
v_txreg                        EQU 0x019a  ; txreg
v_spbrgl                       EQU 0x019b  ; spbrgl
v_spbrgh                       EQU 0x019c  ; spbrgh
v_rcsta                        EQU 0x019d  ; rcsta
v_txsta                        EQU 0x019e  ; txsta
v_txsta_txen                   EQU 0x019e  ; txsta_txen-->txsta:5
v_txsta_brgh                   EQU 0x019e  ; txsta_brgh-->txsta:2
v_baudcon                      EQU 0x019f  ; baudcon
v_baudcon_brg16                EQU 0x019f  ; baudcon_brg16-->baudcon:3
v__pic_temp                    EQU 0x0020  ; _pic_temp-->_pic_state
v__pic_state                   EQU 0x0020  ; _pic_state
v___x_20                       EQU 0x010c  ; x-->lata:0
v___x_21                       EQU 0x010c  ; x-->lata:0
v___data_9                     EQU 0x0023  ; _serial_hw_data_put:data
v___data_3                     EQU 0       ; serial_hw_write_word(): data
v___data_1                     EQU 0x0024  ; serial_hw_write:data
v_usart_div                    EQU 15      ; _calculate_and_set_baudrate(): usart_div
v___n_7                        EQU 0x0025  ; delay_1s:n
v__floop7                      EQU 0x0027  ; delay_1s:_floop7
v__floop8                      EQU 0x0029  ; delay_1s:_floop8
;   24 include 12f1822                    -- target PICmicro
                               org      0
l__main
; D:\ELEKTO~1\JAL\lib/12f1822.jal
;  202 procedure PORTA_low_direction'put(byte in x) is
                               goto     l__l84
; D:\ELEKTO~1\JAL\lib/delay.jal
;  133 procedure delay_1s(word in n) is
 
l__l84
;   44 OSCCON = 0b_0111_1011
                               movlw    123
                               movlb    1
                               movwf    v_osccon
;  881    ANSELA = 0b0000_0000        -- all digital
                               movlb    3
                               clrf     v_ansela
;  888    ADCON0 = 0b0000_0000         -- disable ADC
                               movlb    1
                               clrf     v_adcon0
;  889    ADCON1 = 0b0000_0000
                               clrf     v_adcon1
;  896    CM1CON0 = 0b0000_0000       -- disable comparator
                               movlb    2
                               clrf     v_cm1con0
;  897    CM1CON1 = 0b0000_0000
                               clrf     v_cm1con1
;   48 APFCON_RXDTSEL = true -- Set to RA5
                               bsf      v_apfcon, 7 ; apfcon_rxdtsel
;   49 APFCON_TXCKSEL = true -- Set to RA4
                               bsf      v_apfcon, 2 ; apfcon_txcksel
;   21 end if
                               goto     l__l152


l__calculate_and_set_baudrate
;   54          BAUDCON_BRG16 = true
                               movlb    3
                               bsf      v_baudcon, 3 ; baudcon_brg16
;   55          TXSTA_BRGH = true
                               bsf      v_txsta, 2 ; txsta_brgh
;   69          SPBRG = byte(usart_div)
                               movlw    15
                               movwf    v_spbrgl
;   70          SPBRGH = byte(usart_div >> 8)
                               clrf     v_spbrgh
;  159 end procedure
                               return   
l_serial_hw_init
;   27    _calculate_and_set_baudrate()
                               call     l__calculate_and_set_baudrate
;   30    PIE1_RCIE = false
                               movlb    1
                               bcf      v_pie1, 5 ; pie1_rcie
;   31    PIE1_TXIE = false
                               bcf      v_pie1, 4 ; pie1_txie
;   32    INTCON_GIE = false
                               bcf      v_intcon, 7 ; intcon_gie
;   34    TXSTA_TXEN = true
                               movlb    3
                               bsf      v_txsta, 5 ; txsta_txen
;   38    RCSTA = 0x90
                               movlw    144
                               movwf    v_rcsta
;   40 end procedure
                               return   
;   67 procedure serial_hw_write(byte in data) is
l_serial_hw_write
                               movwf    v___data_1
;   69    while ! PIR1_TXIF loop end loop
l__l114
                               btfsc    v_pir1, 4 ; pir1_txif
                               goto     l__l115
                               goto     l__l114
l__l115
;   71    TXREG = data
                               movf     v___data_1,w
                               movlb    3
                               movwf    v_txreg
;   72 end procedure
                               return   
;  146 procedure serial_hw_data'put(byte in data) is
l__serial_hw_data_put
                               movlb    0
                               movwf    v___data_9
;  147    serial_hw_write(data)
                               movf     v___data_9,w
                               goto     l_serial_hw_write
;  148 end procedure
;  175 end function
l__l152
; D:\Elektornica\RGB_controller\JAL\Blink\12f1822_blink.jal
;   54 serial_hw_init()
                               call     l_serial_hw_init
;   59 pin_A0_direction =  output
                               movlb    1
                               bcf      v_trisa, 0 ; pin_a0_direction
;   61 forever loop
l__l154
;   62    led = on
                               movlb    2
                               bsf      v_lata, 0 ; x20
;   63    serial_hw_data = 0xAA
                               movlw    170
                               movlp    HIGH l__serial_hw_data_put
                               call     l__serial_hw_data_put
;   64    delay_1s(1)
                               movlw    1
                               movlb    0
                               movwf    v___n_7
                               clrf     v___n_7+1
                               movlp    HIGH l_delay_1s
                               call     l_delay_1s
;   65    led = off
                               movlb    2
                               bcf      v_lata, 0 ; x21
;   66    serial_hw_data = 0x55
                               movlw    85
                               movlp    HIGH l__serial_hw_data_put
                               call     l__serial_hw_data_put
;   67    delay_1s(1)
                               movlw    1
                               movlb    0
                               movwf    v___n_7
                               clrf     v___n_7+1
                               movlp    HIGH l_delay_1s
                               call     l_delay_1s
;   68 end loop
                               movlp    HIGH l__l154
                               goto     l__l154
                               end
