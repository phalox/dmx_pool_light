#include <p12F1822.inc>
	list p=12f1822, r=dec
;	errorlevel -306 ; no page boundary warnings
;	errorlevel -302 ; no bank 0 warnings
;	errorlevel -202 ; no 'argument out of range' warnings

	__config _CONFIG1, (_FCMEN_OFF & _IESO_OFF & _CLKOUTEN_OFF & _BOREN_ON & _CPD_ON & _CP_OFF & _MCLRE_ON & _PWRTE_OFF & _WDTE_OFF & _FOSC_INTOSC) ; 0011 1111 1110 0100
	__config _CONFIG2, (_WRT_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_19 & _LVP_OFF) ; 0001 1110 1111 1111

	org 0x000
	nop
	goto init

	;;;;;;;;;;;;;;;;;;;;;
	; Interrupt routine ;
	;;;;;;;;;;;;;;;;;;;;;
	org 0x008
	
	bcf		INTCON, GIE ; Disable interrupts
	bcf		INTCON, T0IF ; Clear Timer0 overflowflag

	movlb	2
	comf	LATA, F ; Invert all LATA pins

	retfie ; Reenable interrupts

init
	; Set clock speed
	movlb	1
	movlw	b'10010111' ; Interne klok op 31.25kHz
	movwf	OSCCON

	clrf	ADCON0 ; Disable ADC
	movlw	b'00000111'
	movwf	ADCON1 ; Digital I/O

	clrf	TRISA ; pin_a5_direction output

	bcf		OPTION_REG, TMR0CS ; Timer0 set to internal clock/4

	movlb	3
	clrf	ANSELA ; All pins digital

	movlb	2
	clrf	CM1CON0 ; Disable comparator
	bsf		LATA, 5
	bsf		LATA, 2

main
	nop
	goto main

	end