#include <p12F1822.inc>
	list p=12f1822, r=dec
;	errorlevel -306 ; no page boundary warnings
;	errorlevel -302 ; no bank 0 warnings
;	errorlevel -202 ; no 'argument out of range' warnings

	__config _CONFIG1, (_FCMEN_OFF & _IESO_OFF & _CLKOUTEN_OFF & _BOREN_ON & _CPD_ON & _CP_OFF & _MCLRE_ON & _PWRTE_OFF & _WDTE_OFF & _FOSC_INTOSC) ; 0011 1111 1110 0100
	__config _CONFIG2, (_WRT_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_19 & _LVP_OFF) ; 0001 1110 1111 1111

	cblock 0x70
	storage
	endc

	org 0x000

	nop
	goto	init

	org 0x004
	;---------------------------------------
	; Interrupt handler
	;---------------------------------------
	bcf		INTCON, GIE

	movlb	3
        ;comf     RCREG, W
        ;movf    RCREG, W
        movlw   128
        subwf   RCREG, W
        movlb   2
        btfsc   STATUS, C
        goto    setLed

        bcf     LATA, 0
        retfie

setLed
        bsf     LATA, 0

	retfie				; and return from interrupt
	;---------------------------------------

init

	; Set clock speed
	;movlw	b'00100011' ; Interne klok op 62.5kHz
	movlw	b'01111011' ; Interne klok op 16Mhz
	movlb	1
	movwf	OSCCON

	movlw	b'10101010'	; Put data in memory
	movwf	storage

	movlb	3
	clrf	ANSELA 		; All pins digital

	movlb	1
	clrf	ADCON0 		; Disable ADC
	clrf	ADCON1 		; Digital I/O

        bcf     TRISA, 0        ; Set RA0 as output

	movlb	2
	clrf	CM1CON0 	; Disable comparator
        clrf    CM1CON1         ;

        bsf     APFCON, RXDTSEL ; Set RX to RA5
        bsf     APFCON, TXCKSEL ; Set TX to RA4
        
	movlb	3

        bsf	BAUDCON, BRG16	; 16 bit baud generator
        bsf     TXSTA, BRGH     ; Set High baud rate

        ; Place 15 in SPBRG<0:15>
	movlw	0x0F
	movwf	SPBRGL

	clrf	SPBRGH

        movlb   1
        bsf     PIE1, RCIE      ; Enable receive intterrupt
        bcf     PIE1, TXIE      ; Disable transmit interrupts

        movlb   3
        bsf     TXSTA, TXEN     ; Enable transmit
        bsf	RCSTA, SPEN	; Enables serial port
        bsf     RCSTA, CREN
        bcf     TXSTA, SYNC     ; Asynch mode

        bsf     INTCON, PEIE    ; Perpheral interrupts enabled
        bsf     INTCON, GIE     ; Reenable global interrupts

        goto    main


	;movlb	1
	;clrf	TRISA 		; All pins output

	;movlb	2
	;clrf	LATA 		; turn off all pins

loopken1
        movlb   0
        btfss   PIR1, TXIF
        goto    loopken1

	movlb	3
	;movf	storage, W
        movlw   b'10101010'
	movwf	TXREG

        ;rlf     storage, F

loopken2
        movlb   0
        btfss   PIR1, TXIF
        goto    loopken2

        movlb   3
        ;movf	storage, W
        movlw   b'01010101'
	movwf	TXREG


	;bsf	INTCON, GIE ; Interrupts on
main
	nop
	goto	main

	end