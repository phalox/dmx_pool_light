#include <p12F1822.inc>
    list p=12f1822, r=dec
;   errorlevel -306 ; no page boundary warnings
;   errorlevel -302 ; no bank 0 warnings
;   errorlevel -202 ; no 'argument out of range' warnings

; Defines addresses
#define R_ADDR_LOW  0x01
#define R_ADDR_HIGH 0x00

; Defines addresses
#define G_ADDR_LOW  0x02
#define G_ADDR_HIGH 0x00

; Defines addresses
#define B_ADDR_LOW  0x03
#define B_ADDR_HIGH 0x00

; Defines starting conditions of pins (useful when no DMX signal is applied)
#define	DUTY_R	0x20
#define	DUTY_G	0xAA
#define	DUTY_B	0xFF

; LED pin locations
#define PIN_R	2
#define PIN_G	1
#define PIN_B	0

; Defines end address (standard is 512)
; The counter is also reset by the break signal, this is an extra check.
; If you want to abuse the DMX-512 protocol (by using more than 512 values),
; Change it here
#define TOTAL_ADDR_LOW  0x00
#define TOTAL_ADDR_HIGH 0x02

; Timer preload that generates timebase for PWM
; This value reduces flickering
#define TIMER_LOAD 0xD0

    __config _CONFIG1, (_FCMEN_OFF & _IESO_OFF & _CLKOUTEN_OFF & _BOREN_ON & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_OFF & _WDTE_OFF & _FOSC_INTOSC) ; 0011 1111 1110 0100
    __config _CONFIG2, (_WRT_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_19 & _LVP_OFF) ; 0001 1110 1111 1111

#define BREAK   0

    cblock 0x70
    cntr        ; 8 bit duty cycle counter
    duty_r
    temp_duty_r
    duty_g
    temp_duty_g
    duty_b
    temp_duty_b
    addr_low    ; Current lower address
    addr_high   ; Current higher address
    temprc      ; Temporary received value
    stat        ; Status register
    endc

    org 0x000
    nop
    goto	init

    org 0x004
    ;---------------------------------------
    ; Interrupt handler
    ;---------------------------------------
    bcf	INTCON, GIE

    movlb   0
    btfsc   PIR1, RCIF
    goto    serialprocedure

    btfsc   INTCON, TMR0IF
    goto    timerprocedure

    

serialprocedure
    movlb	3

    btfss       RCSTA, FERR     ; Test for framing error
    goto        testandinc

    ; A break (or part of the break) has been detected
    movf        RCREG, W        ; Mark register as read
    bsf         stat, BREAK     ; Break has passed, set bit
    movlw       0xFE
    movwf       TXREG
    clrf        addr_low        ; Clear counters
    clrf        addr_high
    retfie


testandinc
    btfss       stat, BREAK     ; Test if break has happened
    goto        serialinc       ; We're past the start sequence

    ; This is the start byte, we don't care about it
    movf        RCREG, W        ; Mark register as read
    bcf         stat, BREAK     ; Clear break bit
    retfie

serialinc
    ; Increment counter
    incf        addr_low, F     ; Increment the Low byte
    btfsc       STATUS, Z       ; Do we have Zero (Multiple of 256)?
    incf        addr_high, F    ; Increment High byte (if necessary)

    movf        RCREG, W        ; Move RCREG in a another variable
    movwf       temprc

    call        debugger

testRedAddress
    movlw	R_ADDR_LOW
    subwf	addr_low, W 	; substract values
    btfss	STATUS, Z 	; if zero detected, skip
    goto        testGreenAddress
    movlw	R_ADDR_HIGH
    subwf	addr_high, W 	; substract values
    btfss	STATUS, Z 	; if zero detected, skip
    goto        testGreenAddress
    ; If this is reached, address is the same
    movf        temprc, W
    movwf       temp_duty_r

testGreenAddress
    movlw	G_ADDR_LOW
    subwf	addr_low, W 	; substract values
    btfss	STATUS, Z 	; if zero detected, skip
    goto        testBlueAddress
    movlw	G_ADDR_HIGH
    subwf	addr_high, W 	; substract values
    btfss	STATUS, Z 	; if zero detected, skip
    goto        testBlueAddress
    ; If this is reached, address is the same
    movf        temprc, W
    movwf       temp_duty_g

testBlueAddress
    movlw	B_ADDR_LOW
    subwf	addr_low, W 	; substract values
    btfss	STATUS, Z 	; if zero detected, skip
    goto        testReset
    movlw	B_ADDR_HIGH
    subwf	addr_high, W 	; substract values
    btfss	STATUS, Z 	; if zero detected, skip
    goto        testReset
    ; If this is reached, address is the same
    movf        temprc, W
    movwf       temp_duty_b

testReset

    movlw	TOTAL_ADDR_LOW
    subwf	addr_low, W 	; substract values
    btfss	STATUS, Z 	; if zero detected, skip
    retfie
    movlw	TOTAL_ADDR_HIGH
    subwf	addr_high, W 	; substract values
    btfss	STATUS, Z 	; if zero detected, skip
    retfie

    ; Address counter has reached maximum
    ; This assures compliance with DMX-512 protocol
    clrf        addr_low        ; Reset counters
    clrf        addr_high
    retfie

timerprocedure
    bcf     INTCON, TMR0IF

    ; Preloading timer value
    movf    TIMER_LOAD, W
    movwf   TMR0

    incfsz  cntr
    goto    int_test

    ; End of period, setting new duty cycles
    movf    temp_duty_r, W
    movwf   duty_r
    movf    temp_duty_g, W
    movwf   duty_g
    movf    temp_duty_b, W
    movwf   duty_b

    ; Setting LEDs
    movlb	2
    movf    duty_r, W
    bcf     LATA, PIN_R ; Turn off first
    btfss   STATUS, Z
    bsf     LATA, PIN_R	; turn on led

    movf    duty_g, W
    bcf     LATA, PIN_G ; Turn off first
    btfss   STATUS, Z
    bsf     LATA, PIN_G	; turn on led

    movf    duty_b, W
    bcf     LATA, PIN_B ; Turn off first
    btfss   STATUS, Z
    bsf     LATA, PIN_B	; turn on led
    retfie

int_test
    movlb	2

    ; Test Red duty
    movf	duty_r, W 	; put duty in W
    subwf	cntr, W 	; substract values
    btfsc	STATUS, Z 	; if zero detected, execute
    bcf         LATA, PIN_R	; turn off led

    ; Test Green duty
    movf	duty_g, W 	; put duty in W
    subwf	cntr, W 	; substract values
    btfsc	STATUS, Z 	; if zero detected, execute
    bcf         LATA, PIN_G	; turn off led

    ; Test Blue duty
    movf	duty_b, W 	; put duty in W
    subwf	cntr, W 	; substract values
    btfsc	STATUS, Z 	; if zero detected, execute
    bcf         LATA, PIN_B	; turn off led

    retfie                      ; and return from interrupt


    ;--------------------------------------------
    ; This is the intialisation and main routine
    ;--------------------------------------------

init

    movlw	0x00
    movwf	cntr		; Reset counter

    movlw	DUTY_R
    movwf	duty_r		; Load duty cycle
    movwf       temp_duty_r

    movlw	DUTY_G
    movwf	duty_g		; Load duty cycle
    movwf       temp_duty_g

    movlw	DUTY_B
    movwf	duty_b		; Load duty cycle
    movwf       temp_duty_b

    clrf        addr_low
    clrf        addr_high

    ; Set clock speed
    movlw	b'01111011'     ; Internal clock at 16Mhz
    movlb	1
    movwf	OSCCON

    movlb	3
    clrf	ANSELA 		; All pins digital

    movlb	1
    clrf	ADCON0 		; Disable ADC
    clrf        ADCON1 		; Digital I/O
    clrf	TRISA 		; All pins output
    bsf         TRISA, 5        ; RA5 is input (UART)

    movlb	2
    clrf	CM1CON0 	; Disable comparator
    clrf        CM1CON1

    bsf     APFCON, RXDTSEL     ; Set RX to RA5
    bsf     APFCON, TXCKSEL     ; Set TX to RA4, only used for debugging purposes

    movlb	3

    ; Configure UART module
    bsf     BAUDCON, BRG16	; 16 bit baud generator
    bsf     TXSTA, BRGH         ; Set High baud rate

    movlw   0x0F                ; Place 15 in SPBRG<0:15>
    movwf   SPBRGL
    clrf    SPBRGH

    movlb   1

    bsf     PIE1, RCIE      ; Enable receive intterrupt
    bcf     PIE1, TXIE      ; Disable transmit interrupts

    movlb   3
    bsf     TXSTA, TXEN     ; Enable transmit
    bsf     RCSTA, RX9      ; 9 bit reading
    bsf     RCSTA, SPEN     ; Enables serial port
    bsf     RCSTA, CREN
    bcf     TXSTA, SYNC     ; Asynch mode

    ; Configure Timer0 module
    movlb   1
    bcf     OPTION_REG, TMR0CS 	; Timer op Fosc/4
    bsf     OPTION_REG, PSA 	; DISABLE prescaler
    bcf     OPTION_REG, PS0 	; Set timer prescaler to 1:2
    bcf     OPTION_REG, PS1
    bcf     OPTION_REG, PS2

    bsf     INTCON, PEIE    ; Perpheral interrupts enabled
    bsf     INTCON, TMR0IE  ; Enable timer0 overflow interrupt
    bsf     INTCON, GIE     ; Interrupts on!

main
    nop
    goto    main

; Writes the current counter values to the transmit UART
debugger
    movlb   3
    movf    addr_high, W
    movwf   TXREG
    movf    addr_low, W
    movwf   TXREG
    return

    end