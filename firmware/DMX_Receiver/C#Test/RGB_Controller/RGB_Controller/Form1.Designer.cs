﻿namespace RGB_Controller
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.s_red = new System.Windows.Forms.TrackBar();
            this.Red = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.s_green = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.s_blue = new System.Windows.Forms.TrackBar();
            this.button1 = new System.Windows.Forms.Button();
            this.form1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.exec = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.s_red)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.s_green)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.s_blue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // s_red
            // 
            this.s_red.LargeChange = 20;
            this.s_red.Location = new System.Drawing.Point(81, 40);
            this.s_red.Maximum = 255;
            this.s_red.Name = "s_red";
            this.s_red.Size = new System.Drawing.Size(287, 45);
            this.s_red.SmallChange = 10;
            this.s_red.TabIndex = 0;
            this.s_red.TickFrequency = 20;
            this.s_red.Scroll += new System.EventHandler(this.s_red_Scroll);
            // 
            // Red
            // 
            this.Red.AutoSize = true;
            this.Red.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Red.ForeColor = System.Drawing.Color.DarkRed;
            this.Red.Location = new System.Drawing.Point(21, 40);
            this.Red.Name = "Red";
            this.Red.Size = new System.Drawing.Size(45, 24);
            this.Red.TabIndex = 1;
            this.Red.Text = "Red";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.label1.Location = new System.Drawing.Point(21, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Green";
            // 
            // s_green
            // 
            this.s_green.LargeChange = 20;
            this.s_green.Location = new System.Drawing.Point(81, 106);
            this.s_green.Maximum = 255;
            this.s_green.Name = "s_green";
            this.s_green.Size = new System.Drawing.Size(287, 45);
            this.s_green.SmallChange = 10;
            this.s_green.TabIndex = 2;
            this.s_green.TickFrequency = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label2.Location = new System.Drawing.Point(21, 171);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 24);
            this.label2.TabIndex = 5;
            this.label2.Text = "Blue";
            // 
            // s_blue
            // 
            this.s_blue.LargeChange = 20;
            this.s_blue.Location = new System.Drawing.Point(81, 171);
            this.s_blue.Maximum = 255;
            this.s_blue.Name = "s_blue";
            this.s_blue.Size = new System.Drawing.Size(287, 45);
            this.s_blue.SmallChange = 10;
            this.s_blue.TabIndex = 4;
            this.s_blue.TickFrequency = 20;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(374, 40);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 85);
            this.button1.TabIndex = 6;
            this.button1.Text = "Send";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // form1BindingSource
            // 
            this.form1BindingSource.DataSource = typeof(RGB_Controller.Form1);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(482, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(262, 296);
            this.textBox1.TabIndex = 7;
            // 
            // exec
            // 
            this.exec.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exec.Location = new System.Drawing.Point(374, 185);
            this.exec.Name = "exec";
            this.exec.Size = new System.Drawing.Size(87, 85);
            this.exec.TabIndex = 8;
            this.exec.Text = "Exec";
            this.exec.UseVisualStyleBackColor = true;
            this.exec.Click += new System.EventHandler(this.exec_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 320);
            this.Controls.Add(this.exec);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.s_blue);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.s_green);
            this.Controls.Add(this.Red);
            this.Controls.Add(this.s_red);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.s_red)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.s_green)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.s_blue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar s_red;
        private System.Windows.Forms.Label Red;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar s_green;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar s_blue;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource form1BindingSource;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button exec;
    }
}

