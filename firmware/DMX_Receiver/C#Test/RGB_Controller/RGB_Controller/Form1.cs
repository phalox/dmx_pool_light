﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace RGB_Controller
{

    public partial class Form1 : Form
    {

        SerialPort comm;
        Timer t, t2;
        Random r;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comm = new SerialPort("COM5", 250000, Parity.None, 8, StopBits.Two);
            try
            {
                comm.Open();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
            }
            /*
            t = new Timer();
            t.Interval = 1;
            t.Tick += new EventHandler(getAndWritet);
            t.Start();

            t2 = new Timer();
            t2.Interval = 200;
            t2.Tick += new EventHandler(t_Tick);
            t2.Start();

            r = new Random();
            */

        }

        void t_Tick(object sender, EventArgs e)
        {
            changeVals();
        }

        void getAndWritet(object sender, EventArgs e)
        {
            getAndWrite();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            comm.Close();
        }

        private void writeTimes(int times)
        {
            for (int i = 0; i < times; i++)
            {

            }
        }

        private void changeVals()
        {
            s_red.Value = randNum(s_red);
            s_green.Value = randNum(s_green);
            s_blue.Value = randNum(s_blue);
        }

        private int randNum(TrackBar t)
        {
            int rand = r.Next(t.Value-10, t.Value+10);
            if(rand < 0)
                return 0;
            if(rand > 255)
                return 255;
            return rand;
        }

        private void getAndWrite()
        {
            writeVals((byte)s_red.Value, (byte)s_green.Value, (byte)s_blue.Value);
        }

        private void writeVals(byte red, byte green, byte blue)
        {
            byte[] toSend = {red, green, blue};
            comm.Write(toSend, 0, 3);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            getAndWrite();
        }

        private void s_red_Scroll(object sender, EventArgs e)
        {
            //getAndWrite();
        }

        private void s_green_Scroll(object sender, EventArgs e)
        {
            //getAndWrite();
        }

        private void s_blue_Scroll(object sender, EventArgs e)
        {
            //getAndWrite();
        }

        private void exec_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 550; i++)
            {
                comm.Write("A");
                while (comm.BytesToRead == 0)
                { }
                int msb = comm.ReadByte();
                while (comm.BytesToRead == 0)
                { }
                int lsb = comm.ReadByte();
                textBox1.AppendText(i.ToString() + " => " + msb.ToString("X") + lsb.ToString("X") + "\r\n");
            }
        }
    }
}
