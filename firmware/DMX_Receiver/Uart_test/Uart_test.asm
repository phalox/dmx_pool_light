#include <p12F1822.inc>
	list p=12f1822, r=dec
;	errorlevel -306 ; no page boundary warnings
;	errorlevel -302 ; no bank 0 warnings
;	errorlevel -202 ; no 'argument out of range' warnings

	__config _CONFIG1, (_FCMEN_OFF & _IESO_OFF & _CLKOUTEN_OFF & _BOREN_ON & _CPD_ON & _CP_OFF & _MCLRE_ON & _PWRTE_OFF & _WDTE_OFF & _FOSC_INTOSC) ; 0011 1111 1110 0100
	__config _CONFIG2, (_WRT_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_19 & _LVP_OFF) ; 0001 1110 1111 1111

	cblock 0x70
	storage
	endc

	org 0x000
	nop
	goto	init
	
	org 0x004
	;---------------------------------------
	; Interrupt handler
	;---------------------------------------
	bcf		INTCON, GIE

	movlb	3
	movf	storage, W
	movwf	TXREG

	rlf		storage, F

	retfie				; and return from interrupt
	;---------------------------------------

init

	; Set clock speed
	;movlw	b'10100111' ; Interne klok op 62.5kHz
	movlw	b'11111111' ; Interne klok op 16Mhz
	movlb	1
	movwf	OSCCON

	movlw	b'10101010'	; Send inverting data!
	movwf	storage

	movlb	3
	clrf	ANSELA 		; All pins digital

	movlb	1
	clrf	ADCON0 		; Disable ADC
	movlw	b'00000111'
	movwf	ADCON1 		; Digital I/O

	movlb	2
	clrf	CM1CON0 	; Disable comparator

	movlb	1
	clrf	TRISA 		; All pins output

	movlb	2
	clrf	LATA 		; turn off all pins


	;bsf		INTCON, TMR0IE 		; Enable timer0 overflow interrupt
	
	movlb	1

	bcf		OPTION_REG, TMR0CS 	; Timer op Fosc/4
	bsf		OPTION_REG, PSA 	; DISABLE prescaler
	bsf		OPTION_REG, PS0 	; Set timer prescaler to 1:256
	bcf		OPTION_REG, PS1
	bcf		OPTION_REG, PS2

	movlb	3

	;8 bit transmission
	;Transmit enabled
	;Asynch mode
	;High speed baud rate
	;movlw	b'00100100'
	;movwf	TXSTA

	clrf	SPBRGH	; Clear high register
	movlw	0x0D	; Place 15 in low
	movwf	SPBRGL
	
        bsf	BAUDCON, BRG16	; 16 bit baud generator

        bcf     TXSTA, SYNC     ; Asynch mode

	bsf	RCSTA, SPEN	; Enables serial port

        bsf     TXSTA, TXEN     ; Enable serial port

	bsf	PIE1, TXIE	; Enable serial transmit interrupt

	movlb	3
	movf	storage, W
	movwf	TXREG

	bsf	INTCON, GIE ; Interrupts on!
main
	nop
	goto	main

	end