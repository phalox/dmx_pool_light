#include <p12F1822.inc>
	list p=12f1822, r=dec
;	errorlevel -306 ; no page boundary warnings
;	errorlevel -302 ; no bank 0 warnings
;	errorlevel -202 ; no 'argument out of range' warnings
; LED pin locations
#define PIN_R	0
#define PIN_G	2
#define PIN_B	1



#define	DUTY_R	0x01
#define	DUTY_G	0xFF
#define	DUTY_B	0xFF


	__config _CONFIG1, (_FCMEN_OFF & _IESO_OFF & _CLKOUTEN_OFF & _BOREN_ON & _CPD_ON & _CP_OFF & _MCLRE_ON & _PWRTE_OFF & _WDTE_OFF & _FOSC_INTOSC) ; 0011 1111 1110 0100
	__config _CONFIG2, (_WRT_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_19 & _LVP_OFF) ; 0001 1110 1111 1111

	cblock 0x70
	teller
	duty_r
        duty_g
        duty_b
	endc

	org 0x000
	nop
	goto	init

	org 0x004
	;---------------------------------------
	; Interrupt handler
	;---------------------------------------
	bcf		INTCON, GIE
	bcf		INTCON, T0IF

	incfsz	teller
	goto	int_test

	; End of period: Setting LED
	movlb	2
        movf    duty_r, W
        btfss   STATUS, Z
	bsf	LATA, PIN_R	; turn on led

        movf    duty_g, W
        btfss   STATUS, Z
        bsf	LATA, PIN_G	; turn on led

        movf    duty_b, W
        btfss   STATUS, Z
        bsf	LATA, PIN_B	; turn on led
	retfie 				; Go back!

int_test
	movlb	2

	; Test Red duty
	movf	duty_r, W 	; put duty in W
	subwf	teller, W 	; substract values
	btfsc	STATUS, Z 	; if zero detected, skip
	bcf	LATA, PIN_R	; turn off led

	; Test Green duty
	movf	duty_g, W 	; put duty in W
	subwf	teller, W 	; substract values
	btfsc	STATUS, Z 	; if zero detected, skip
	bcf	LATA, PIN_G	; turn off led

	; Test Blue duty
	movf	duty_b, W 	; put duty in W
	subwf	teller, W 	; substract values
	btfsc	STATUS, Z 	; if zero detected, skip
	bcf	LATA, PIN_B	; turn off led

	retfie				; and return from interrupt
	;---------------------------------------

init
	movlw	0x00
	movwf	teller		; Reset teller

	movlw	DUTY_R
	movwf	duty_r		; Load duty cycle

	movlw	DUTY_G
	movwf	duty_g		; Load duty cycle

        movlw	DUTY_B
	movwf	duty_b		; Load duty cycle

	; Set clock speed
	;movlw	b'10100111' ; Interne klok op 62.5kHz
	movlw	b'11111111' ; Interne klok op 16Mhz
	movlb	1
	movwf	OSCCON

	movlb	3
	clrf	ANSELA 		; All pins digital

	movlb	1
	clrf	ADCON0 		; Disable ADC
	movlw	b'00000111'
	movwf	ADCON1 		; Digital I/O

	movlb	2
	clrf	CM1CON0 	; Disable comparator
	bsf		LATA, 5

	movlb	1
	clrf	TRISA 		; All pins output

	movlb	2
	clrf	LATA 		; turn off all LEDs


	bsf		INTCON, TMR0IE 		; Enable timer0 overflow interrupt

	movlb	1
	bcf		OPTION_REG, TMR0CS 	; Timer op Fosc/4
	bsf		OPTION_REG, PSA 	; DISABLE prescaler
	bsf		OPTION_REG, PS0 	; Set timer prescaler to 1:256
	bcf		OPTION_REG, PS1
	bcf		OPTION_REG, PS2

	bsf		INTCON, GIE ; Interrupts on!
main
	nop
	goto	main

	end