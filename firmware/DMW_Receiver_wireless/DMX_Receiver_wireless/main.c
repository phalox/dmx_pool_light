/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/
#define SYS_FREQ        32000000L
#define FCY             SYS_FREQ/2
/* Device header file */
#if defined(__PIC24E__)
	#include <p24Exxxx.h>
#elif defined(__PIC24F__)
	#include <p24Fxxxx.h>
#elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
#endif

_CONFIG1(JTAGEN_OFF & GCP_OFF & GWRP_OFF & BKBUG_ON & ICS_PGx1
        & FWDTEN_OFF & WINDIS_ON & FWPSA_PR32 & WDTPS_PS32768);
_CONFIG2(IESO_ON & WUTSEL_LEG & SOSCSEL_SOSC & FNOSC_FRCPLL
        & FCKSM_CSDCMD & OSCIOFNC_ON & IOL1WAY_OFF & I2C1SEL_PRI & POSCMOD_NONE);



#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>       /* Includes true/false definition                  */
#include <stdlib.h>
#include <xc.h>
#include <libpic30.h>

#include "wl_module.h"
#include "dmx.h"

/******************************************************************************/
/* Global Variable Declaration                                                */
/******************************************************************************/

#define R 0
#define G 1
#define B 2

uint8_t dmx_values[NUM_STRIPS*NUM_COLORS];

uint16_t counter = 0;
uint16_t dmx_progress = 0;
uint16_t num_dmx_values = NUM_STRIPS*NUM_COLORS;

/****************************************************************************/

int16_t main(void)
{
    unsigned char payload[wl_module_PAYLOAD]; //holds the payload
    unsigned char nRF_status; //STATUS information of nRF24L01+
    /*
     * Set clock speed to 32MHz => instruction clock to 16MHz
     */
    CLKDIVbits.RCDIV = 0b000;   //Prescaler 1: CLKDIV<10:8> Default: 001 = 4Mhz output | 000 = 8Mhz
    CLKDIVbits.DOZEN = 0b0; // Enable prescaler
    //CLKDIVbits.DOZE = 0b010;    //Prescaler 2: CLKDIR<14:12> Default: 011 = 1:8 | 000 = 1:1
    OSCCON = 0x11C0;	 //select INTERNAL RC, Post Scale PPL

    /* Initialize IO ports and peripherals */
    AD1PCFG = 0xFFFF; // Disable Analog inputs
    TRISB = 0X0000; // All pins output
    TRISA = 0X0000; // All pins output

    // Configure nrf module
    wl_module_init();	//initialise nRF24L01+ Module
    __delay_ms(50);	//wait for nRF24L01+ Module

    wl_module_config(); // Config module


    while(1)
    {
      while (!wl_module_data_ready()); //waits for RX_DR Flag in STATUS
      nRF_status = wl_module_get_data(payload); //reads the incomming Data to Array payload
      
    }
}

//_CNInterrupt() is the Change Notifcation interrupt service routine (ISR).
void __attribute__((__interrupt__, auto_psv)) _CNInterrupt(void)
{
  if(!PORTBbits.RB2)
  {
    // Interrupt occured
  }
  _CNIF = 0; // Clear interrupt flag
}