/*
 *      2013 Elia Ritterbusch
 *      http://eliaselectronics.com
  *
 *      2013 Phalox.be
 *      Modified to work with PIC24F series
 *
 *      This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 *      To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
 */
#include <xc.h>
#include "spi.h"
#include <spi.h> // Microchip header file



void spi_init(void){
    /* SPI init*/

  // SDO1 7 SPI1 Data Output
  // SCK1OUT 8 SPI1 Clock Output
  // SS1OUT 9 SPI1 Slave Select Out

  SPI1_SCL_RP_REG = 8; // Clock out
  SPI1_MOSI_RP_REG = 7; // Data out


  // SPI1 Data Input SDI1 RPINR20 SDI1R<4:0>
  SPI1_MISO_TRIS = 1; // set SPI1 SDI pin to input
  RPINR20bits.SDI1R = SPI1_MISO_RP; // SPI1 SDI

  // 8 bit mode
  // Input data sampled at middle
  // Clock idle low
  // Change values on falling edge

  //SPI1CON1 = 0x1A; // Primary scale 4:1, Secondary scale 2:1 => 16Mhz:8 = 2Mhz
  //SPI1CON1 = 0x1B; // Primary scale 1:1, Secondary scale 2:1 => 16Mhz:2 = 8Mhz
  SPI1CON1 = 0x1E; // Primary scale 4:1, Secondary scale 1:1 => 16Mhz:4 = 4Mhz
  SPI1CON1bits.MSTEN = 1;
  SPI1CON1bits.CKP = 0;
  SPI1CON1bits.CKE = 1;
  SPI1CON1bits.SMP = 1;

  SPI1CON2 = 0;

  SPI1STAT = 0; // Clear SPI
  SPI1STATbits.SPIEN = 1;
}

void spi_transmit_sync(unsigned char * data, unsigned int length){
    unsigned char tmp;
    while(length){
	SPI1BUF = *data;
	while (!SPI1_Rx_Buf_Full);
        tmp = SPI1BUF; // read out data
        length--;
	data++;
    }
}

void spi_transmit_sync_hdr_data(unsigned char * hdr_data, unsigned int hdr_length, unsigned char * data, unsigned int length){
    unsigned char tmp;
    // First send header
    while(hdr_length--){
	SPI1BUF = *(hdr_data++);
	while (!SPI1_Rx_Buf_Full);
        tmp = SPI1BUF; // read out data
    }
    // Then send actual data
    while(length--){
	SPI1BUF = *(data++);
	while (!SPI1_Rx_Buf_Full);
        tmp = SPI1BUF; // read out data
    }
}

void spi_transfer_sync(unsigned char * dataout, unsigned char * datain, unsigned int length){
    while(length){
	SPI1BUF = *dataout;
	while (!SPI1_Rx_Buf_Full);
        *datain = SPI1BUF; // read out data
        length--;
	dataout++;
        datain++;
    }
}

unsigned char spi_fast_shift(unsigned char data){
    SPI1BUF = data;
    while (!SPI1_Rx_Buf_Full);
    return SPI1BUF;
}