/* 
 * File:   dmx.h
 * Author: Toon
 *
 * Created on 9 juni 2013, 14:50
 */

#ifndef DMX_H
#define	DMX_H

#define START_BYTE 0

#define NUM_STRIPS 4
#define NUM_COLORS 3

#endif	/* DMX_H */

