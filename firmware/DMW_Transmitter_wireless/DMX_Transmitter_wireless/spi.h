/*
 *      2013 Elia Ritterbusch
 *      http://eliaselectronics.com
 *
 *      2013 Phalox.be
 *      Modified to work with PIC24F series
 *
 *      This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 *      To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
 */
#ifndef _SPI_H_
#define	_SPI_H_

#define SPI1_CE              RA0
#define SPI1_CE_TRIS         TRISAbits.TRISA0
#define SPI1_CE_LAT          LATAbits.LATA0

#define SPI1_CS              RA1
#define SPI1_CS_TRIS         TRISAbits.TRISA1
#define SPI1_CS_LAT          LATAbits.LATA1

#define SPI1_IRQ             RB2
#define SPI1_IRQ_TRIS        TRISBbits.TRISB2
#define SPI1_IRQ_LAT         LATBbits.LATB2
#define SPI1_IRQ_CN          6

#define SPI1_SCL             RB13
#define SPI1_SCL_RP          13
#define SPI1_SCL_RP_REG      RPOR6bits.RP13R
#define SPI1_SCL_TRIS        TRISBbits.TRISB13
#define SPI1_SCL_LAT         LATBbits.LATB13

#define SPI1_MISO            RB15
#define SPI1_MISO_RP         15
#define SPI1_MISO_TRIS       TRISBbits.TRISB15
#define SPI1_MISO_LAT        LATBbits.LATB15

#define SPI1_MOSI            RB14
#define SPI1_MOSI_RP         14
#define SPI1_MOSI_RP_REG     RPOR7bits.RP14R
#define SPI1_MOSI_TRIS       TRISBbits.TRISB14
#define SPI1_MOSI_LAT        LATBbits.LATB14

#define SPI1_SEL()       SPI1_CS_LAT = 0;
#define SPI1_DESEL()     SPI1_CS_LAT = 1;

extern void spi_init(void);
extern void spi_transfer_sync(unsigned char *dataout, unsigned char *datain, unsigned int length);
extern void spi_transmit_sync_hdr_data(unsigned char *hdr_data, unsigned int hdr_length, unsigned char *data, unsigned int length);
extern void spi_transmit_sync(unsigned char *data, unsigned int length);
extern unsigned char spi_fast_shift(unsigned char data);

#endif	/* _SPI_H_ */

